# Import SDK packages
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import time
import json
import pandas as pd
import numpy as np
import csv


#TODO 1: modify the following parameters
#Starting and end index, modify this
device_st = 0
device_end = 5

#Path to the dataset, modify this
data_path = "C:/Users/Yum/Documents/School/CS_437/AWS_IoT/vehicle_data/vehicle{}.csv"
data_json = "C:/Users/Yum/Documents/School/CS_437/AWS_IoT/vehicle_data/vehicle{}.json"

#Path to your certificates, modify this
certificate_formatter = "C:/Users/Yum/Documents/School/CS_437/AWS_IoT/certificates/device_{}/device_{}.certificate.pem"
key_formatter = "C:/Users/Yum/Documents/School/CS_437/AWS_IoT/certificates/device_{}/device_{}.private.pem"


class MQTTClient:
    def __init__(self, device_id, cert, key):
        # For certificate based connection
        self.device_id = str(device_id)
        self.state = 0
        self.client = AWSIoTMQTTClient(self.device_id)
        #TODO 2: modify your broker address
        self.client.configureEndpoint("a199gqn5ty0hbk-ats.iot.us-east-2.amazonaws.com", 8883)
        self.client.configureCredentials("C:/Users/Yum/Documents/School/CS_437/AWS_IoT/Amazon-root-CA-1.pem", key, cert)
        self.client.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
        self.client.configureDrainingFrequency(2)  # Draining: 2 Hz
        self.client.configureConnectDisconnectTimeout(10)  # 10 sec
        self.client.configureMQTTOperationTimeout(5)  # 5 sec
        self.client.onMessage = self.customOnMessage
        

    def customOnMessage(self,message):
        #TODO3: fill in the function to show your received message
        print("client {} received payload {} from topic {}".format(self.device_id, message.payload, message.topic))


    # Suback callback
    def customSubackCallback(self,mid, data):
        #You don't need to write anything here
        pass


    # Puback callback
    def customPubackCallback(self,mid):
        #You don't need to write anything here
        pass


    def publish(self, Payload="test/payload"):
        #TODO4: fill in this function for your publish
        #self.client.subscribeAsync("iot/test", 0, ackCallback=self.customSubackCallback)
        self.client.subscribeAsync("test/publish", 0, ackCallback=self.customSubackCallback)
        #self.client.publishAsync("iot/test", Payload, 0, ackCallback=self.customPubackCallback)
        self.client.publishAsync("test/publish", Payload, 0, ackCallback=self.customPubackCallback)
        time.sleep(0.5)

def make_json(csvFilePath, jsonFilePath):
     
    # create a dictionary
    data = {}
     
    # Open a csv reader called DictReader
    with open(csvFilePath, encoding='utf-8') as csvf:
        csvReader = csv.DictReader(csvf)
         
        # Convert each row into a dictionary 
        # and add it to data
        for rows in csvReader:
             
            # Assuming a column named 'No' to
            # be the primary key
            key = rows['timestep_time']
            data[key] = rows
 
    # Open a json writer, and use the json.dumps() 
    # function to dump data
    with open(jsonFilePath, 'w', encoding='utf-8') as jsonf:
        jsonf.write(json.dumps(data, indent=4))

print("Loading vehicle data...")
data = []
for i in range(5):
    csvFilePath = data_path.format(i)
    jsonFilePath = data_json.format(i)
    # a = pd.read_csv(csvFilePath)
    make_json(csvFilePath, jsonFilePath)
    f = open(jsonFilePath)
    json_data = json.load(f)
    for row in json_data:
        data.append(json.dumps(json_data[row]))

print("Initializing MQTTClients...")
clients = []
for device_id in range(device_st, device_end):
    client = MQTTClient(device_id,certificate_formatter.format(device_id,device_id) ,key_formatter.format(device_id,device_id))
    client.client.connect()
    clients.append(client)
 

while True:
    print("send now?")
    x = input()
    if x == "s":
        for i,c in enumerate(clients):
            '''
            for row in data:
                c.publish(Payload = row)
            '''
            c.publish(Payload = '{"test": "Sent from device"}')
            break

    elif x == "d":
        for c in clients:
            c.client.disconnect()
        print("All devices disconnected")
        exit()
    else:
        print("wrong key pressed")

    time.sleep(3)





